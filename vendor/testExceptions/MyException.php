<?php
namespace MyExceptions;
use MyExceptions\Interfaces\I_MyException;

class MyException extends \Exception implements I_MyException
{
	public function __construct(string $message)
	{
		parent::__construct($message);
	}
}