<?php
namespace Logger;
use Logger\DefaultLogger;
/**
 * Log Errors to error.log
 */
class ErrorLogger extends DefaultLogger
{
	const LOG_FILE = "logs/error.log";

	/**
	 * Save message to error log
	 * @return boolean
	 */
	public function log() : bool
	{
		parent::log();
		$save = file_put_contents(self::LOG_FILE, $this->logMessage, FILE_APPEND);

		return ($save) ? true : false;
	}
}