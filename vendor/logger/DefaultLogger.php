<?php
namespace Logger;
use Logger\interfaces\I_Logger;
/**
 * Log all throwable types to default.log
 * Parent for other loggers
 */
class DefaultLogger implements I_Logger
{
	const DEFAULT_LOG = "logs/default.log";

	protected $throwable;
	protected $throwableClass;
	protected $logMessage;

	function __construct(\Throwable $throwable)
	{
		$this->throwable 		= $throwable;
		$this->throwableClass	= get_class($throwable);

		$this->setLogMessage();
	}

	/**
	 * Set date, throwable type, message
	 *
	 * @return self
	 */
	protected function setLogMessage() : self
	{
		$date = date("d-m-Y H:i:s");

		$this->logMessage = 
		"{$date}\r\n{$this->throwableClass}\r\n{$this->throwable->getMessage()}\r\n
		============================================\r\n";

		return $this;
	}

	/**
	 * Save message to default log
	 * @return boolean
	 */
	public function log() : bool
	{
		$save = file_put_contents(self::DEFAULT_LOG, $this->logMessage, FILE_APPEND);

		return ($save) ? true : false;
	}
}