<?php
namespace Logger;
use Logger\Interfaces\I_Logger;
use MyExceptions\Interfaces\I_MyException;
use Logger\MyExceptionLogger;
/**
 * Create Logger by Throwable type
 */
class LoggerFactory
{
	private function __construct()
	{}

	static public function create(\Throwable $e) : I_Logger
	{
		if ($e instanceof I_MyException)
			return new MyExceptionLogger($e);
		if ($e instanceof \Error)
			return new ErrorLogger($e);
		if ($e instanceof \Exception)
			return ExceptionLogger($e);
	}
}