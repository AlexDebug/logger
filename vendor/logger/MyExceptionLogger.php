<?php
namespace Logger;
use Logger\DefaultLogger;
/**
 * Log MyExceptions to myException.log
 */
class MyExceptionLogger extends DefaultLogger
{
	const LOG_FILE = "logs/myException.log";

	/**
	 * Save message to myException log
	 * @return boolean
	 */
	public function log() : bool
	{
		parent::log();
		$save = file_put_contents(self::LOG_FILE, $this->logMessage, FILE_APPEND);

		return ($save) ? true : false;
	}
}