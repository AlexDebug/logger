<?php
namespace Logger\Interfaces;
/**
 * Logger Interface
 */
interface I_Logger
{
	public function log() : bool;
}