<?php
namespace Logger;
use Logger\DefaultLogger;
/**
 * Log System Exceptions to exception.log
 */
class ExceptionLogger extends DefaultLogger
{
	const LOG_FILE = "logs/exception.log";

	/**
	 * Save message to exception log
	 * @return boolean
	 */
	public function log() : bool
	{
		parent::log();
		$save = file_put_contents(self::LOG_FILE, $this->logMessage, FILE_APPEND);

		return ($save) ? true : false;
	}
}