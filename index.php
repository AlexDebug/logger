<?php
require_once "vendor/autoload.php";
use Logger\TestException;
use Logger\LoggerFactory;

try {
	$r = new TestException(1);
}
catch(\Throwable $e) {
	var_dump(LoggerFactory::create($e)->log());
}